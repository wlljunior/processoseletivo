package br.com.experian.processoSeletivo.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.experian.processoSeletivo.data.ReportTopClienteOperadora;
import br.com.experian.processoSeletivo.model.Evento;
import br.com.experian.processoSeletivo.service.EventoService;

@RestController
public class ClientController {
	
	private EventoService eventoService;
	
	@Autowired
	public ClientController(EventoService eventoService) {
		super();
		this.eventoService = eventoService;
	}
    
	@CrossOrigin
	@GetMapping("/client")
	public ResponseEntity<List<ReportTopClienteOperadora>>  getLast30min() throws Exception{		
		
		
		List<Evento> eventos = eventoService.findLast30mim();
		
		if (eventos == null || eventos.isEmpty()) {
			throw new Exception("Nenhum registro encontrado");
		}
		
		
		return new ResponseEntity<List<ReportTopClienteOperadora>>(getTopClientesOperadoras(eventos), HttpStatus.OK);
	}
	
	private List<ReportTopClienteOperadora> getTopClientesOperadoras(List<Evento> eventos) {
		
		ArrayList<ReportTopClienteOperadora> report = new ArrayList<ReportTopClienteOperadora>();
		
		ArrayList<String> operadoras = new ArrayList<>(Arrays.asList("visa", "mastercard", "elo", "americanexpress"));
		
		for (String operadora : operadoras) {
			try {
				Map<String, Double> map = eventos.stream()
						  .collect(Collectors.groupingBy(e -> e.getOperadora()+":"+e.getCliente(),
						                                    Collectors.summingDouble(e->e.getValor())));
	
				Optional<Entry<String, Double>> optional = map.entrySet().stream().filter(m -> m.getKey().startsWith(operadora)).sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).findFirst();
				
				
				String sOperadora = optional.get().getKey().split(":")[0];
				String sCliente = optional.get().getKey().split(":")[1];
				Double valor = optional.get().getValue();
				
				report.add(new ReportTopClienteOperadora(sOperadora, sCliente, valor));
			}catch (Exception e) {
				// TODO: Exceção silenciosa
			}
		}
		
		return report;
	}
    
    
}
