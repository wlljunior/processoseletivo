package br.com.experian.processoSeletivo.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.experian.processoSeletivo.model.Usuario;

public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Long>{
	
	Optional<Usuario> findByUsername(String username);

}
