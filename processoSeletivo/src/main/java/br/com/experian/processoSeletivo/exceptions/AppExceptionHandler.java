package br.com.experian.processoSeletivo.exceptions;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler{

		@ExceptionHandler(value = {Exception.class})
		public ResponseEntity<Object> handleAnyException(Exception e, WebRequest request){	
			
			AppExceptionResponse appExceptionResponse = new AppExceptionResponse(new Date(), 
																				 e.getLocalizedMessage() != null ? e.getLocalizedMessage() : e.toString());
			
			return new ResponseEntity<>(appExceptionResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);			
		}
		
}
