package br.com.experian.processoSeletivo.service;


import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.experian.processoSeletivo.model.Evento;
import br.com.experian.processoSeletivo.repository.EventoRepository;




@Service
public class EventoServiceImpl implements EventoService {

	@Autowired
	EventoRepository eventoRepostory;
	
	@Transactional
	public Evento save(Evento evento) {
		return eventoRepostory.save(evento);
	}
	
	public List<Evento> findLast30mim() {
		
		int addMinuteTime = -30;
		Date targetTime = Calendar.getInstance().getTime();
		targetTime = DateUtils.addMinutes(targetTime, addMinuteTime); // add minute
		
		
		
		return eventoRepostory.findLast30mim(targetTime);		
		
	}
	
}
