package br.com.experian.processoSeletivo.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.experian.processoSeletivo.data.CustomUserDetail;
import br.com.experian.processoSeletivo.model.Usuario;

public class JWTAuthenticateFilter extends UsernamePasswordAuthenticationFilter{
	
	public static final int EXPIRE_TOKEN = 42_000;	
	public static final String TOKEN = "f38fefeb-96d8-49f8-bb5f-4c8d9130cb9e";
	
	private final AuthenticationManager authenticationManager;
	
	public JWTAuthenticateFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request,
            									HttpServletResponse response) throws AuthenticationException {
		
		try {
			
			Usuario user = new ObjectMapper().readValue(request.getInputStream(), Usuario.class);
			
			return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					user.getUsername(), 
					user.getPassword(), 
					new ArrayList<>()));
			
		} catch (IOException e) {
			throw new RuntimeException("Falha ao autenticar", e);
		}
		
		
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request,
							                HttpServletResponse response,
							                FilterChain chain,
							                Authentication authResult) throws IOException, ServletException {
		
		CustomUserDetail userDetails = (CustomUserDetail) authResult.getPrincipal();
		
		String token = JWT.create()
							.withSubject(userDetails.getUsername())
								.withExpiresAt(new Date(System.currentTimeMillis() + EXPIRE_TOKEN))
									.sign(Algorithm.HMAC512(TOKEN));
		
		response.getWriter().write(token);
		response.getWriter().flush();
	}

}
