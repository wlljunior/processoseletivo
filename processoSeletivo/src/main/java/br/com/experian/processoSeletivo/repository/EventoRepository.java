package br.com.experian.processoSeletivo.repository;



import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.experian.processoSeletivo.model.Evento;

public interface EventoRepository extends PagingAndSortingRepository<Evento, Long>{
	
	@Query(value = "select *"
			     + "  from tb_evento "
			     + " where data >= ?1",
		   nativeQuery = true)
	List<Evento> findLast30mim(Date data);
	
	
	List<Evento> findByIdEvento(Long idEvento);
	
	
}
