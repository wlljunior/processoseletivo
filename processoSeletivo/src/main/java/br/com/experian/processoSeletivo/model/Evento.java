package br.com.experian.processoSeletivo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "TB_EVENTO")
public class Evento {
	
	@Id
	@GeneratedValue
	@Column(name = "ID_EVENTO", nullable=false)
	private Long idEvento;
	
	@Column(name = "OPERADORA", nullable=false)
	private String operadora;
	
	@Column(name = "VALOR")
	private Double valor;
	
	@Column(name = "DATA")
	private Date data;
	
	@Column(name = "CLIENTE")
	private String cliente;

	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public String getOperadora() {
		return operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	@Override
	public String toString() {
		return "Evento [idEvento=" + idEvento + ", operadora=" + operadora + ", valor=" + valor + ", data=" + data
				+ ", cliente=" + cliente + "]";
	}

}
