package br.com.experian.processoSeletivo.service;


import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import br.com.experian.processoSeletivo.data.CustomUserDetail;
import br.com.experian.processoSeletivo.model.Usuario;
import br.com.experian.processoSeletivo.repository.UsuarioRepository;

@Component
public class CustomUserDatailService implements UserDetailsService {

	private final UsuarioRepository repository;
	

    public CustomUserDatailService(UsuarioRepository repository) {
        this.repository = repository;
    }
    
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Optional<Usuario> user = repository.findByUsername(username);
		
		if (user.isEmpty()) {
            throw new UsernameNotFoundException("Usuário [" + username + "] não encontrado");
        }
		
		return new CustomUserDetail(user);
		
	}

}
