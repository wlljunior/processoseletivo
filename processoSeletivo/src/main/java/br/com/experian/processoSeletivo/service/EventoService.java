package br.com.experian.processoSeletivo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.experian.processoSeletivo.model.Evento;

@Service
public interface EventoService{

	Evento save(Evento evento);

	List<Evento> findLast30mim();
	
}
