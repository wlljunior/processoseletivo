package br.com.experian.processoSeletivo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "TB_USER")
public class Usuario {
	
	@Id
	@Column(name = "USER_NAME", nullable=false)
	private String username;

	
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@Column(name = "PASSWORD", nullable=false)
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
