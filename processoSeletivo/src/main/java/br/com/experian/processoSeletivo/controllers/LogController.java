package br.com.experian.processoSeletivo.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.experian.processoSeletivo.model.Evento;
import br.com.experian.processoSeletivo.service.EventoService;

@RestController
public class LogController {
	
	private EventoService eventoService;
	
	@Autowired
	public LogController(EventoService eventoService) {
		super();
		this.eventoService = eventoService;
	}
    
	@CrossOrigin
	@PostMapping("/log")
	public ResponseEntity<String> createEvento(@RequestBody Evento evento) throws Exception {
		
			validateEvento(evento);
		
			evento.setData(Calendar.getInstance().getTime());
			eventoService.save(evento);
			
			return new ResponseEntity<>(HttpStatus.OK);
	}
	
	private void validateEvento(Evento evento) throws Exception {
		
		ArrayList<String> operadoras = new ArrayList<>(Arrays.asList("visa", "mastercard", "elo", "americanexpress"));
		
		if (evento.getOperadora() == null || !operadoras.contains(evento.getOperadora())) {
			throw new Exception("Operadora inválida.");
		}else		
		if (evento.getValor() == null || evento.getValor() <= 0.0) {
			throw new Exception("Valor do evento menor ou igual a 0.");
		}else 
		if (evento.getCliente() == null) {
			throw new Exception("Cliente não informado.");
		}else 
		if (evento.getCliente().trim().isEmpty() || !evento.getCliente().startsWith("client")) {
			throw new Exception("Cliente deve começar com o prefixo 'client'.");
		}
		
	}
    
    
}
