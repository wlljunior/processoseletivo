package br.com.experian.processoSeletivo.data;

public class ReportTopClienteOperadora {
	
	private String operadora;
	
	private String cliente;
	
	private Double valorTotal;
	
	public ReportTopClienteOperadora() {

	}

	public ReportTopClienteOperadora(String operadora, String cliente, Double valorTotal) {
		this.operadora = operadora;
		this.cliente = cliente;
		this.valorTotal = valorTotal;
	}

	public String getOperadora() {
		return operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	

}
